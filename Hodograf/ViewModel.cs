﻿using Microsoft.Practices.Prism.Commands;
using System;
using System.Windows.Input;
using System.Windows.Threading;
using OxyPlot;
using OxyPlot.Series;
using System.Windows.Media;
using System.Windows;

namespace PSW1
{
    class ViewModel : ViewModelBase
    {
        #region VARIABLE
        private Matrix _rotationMatrix;
        private const double CircleX = 200, CircleY = 250;
        private DateTime _valueTimePrev, _startTime;
        private DispatcherTimer _simulationTimer;
        private bool _simulationNotStarted;
        private double _radius, _length, _velocity, _epsilon, _valueX, _valueXPrev, _valueXPrevPrev, _valueXt, _valueXtt, _timer;
        private double _m1x, _m2x, _m3x, _m1y, _m2y, _m3y, _boxX, _mergeX, _mergeY, _lengthFixed, _boxDelta;
        private double _m1xOryg, _m2xOryg, _m3xOryg, _m1yOryg, _m2yOryg, _m3yOryg, _mergeXOryg, _mergeYOryg;
        private int _delay = 0;
        private Point[] middles;
        private GaussianRandom generator;
        #endregion
        #region PROPERTIES
        public PlotModel ModelX
        { get; private set; }
        public PlotModel ModelXt
        { get; private set; }
        public PlotModel ModelXtt
        { get; private set; }
        public PlotModel ModelXXt
        { get; private set; }
        public bool SimulationStarted
        {
            get { return _simulationNotStarted; }
            set
            {
                if (_simulationNotStarted != value)
                {
                    _simulationNotStarted = value;
                    OnPropertyChanged("SimulationStarted");
                }
            }
        }
        public string Radius
        {
            get { return _radius.ToString(); }
            set
            {
                if (_radius.ToString() != value)
                {
                    if (value.Length != 0)
                        _radius = double.Parse(value);
                    else
                        _radius = 0;

                    if (_length < _radius)
                    {
                        Length = (_radius + 0.02).ToString();
                        OnPropertyChanged("Length");
                    }

                    CircleRadius = _radius;
                    RefreshCirclesMiddle();
                    UpdateBoxPosition();
                    OnPropertyChanged("Radius");
                    OnPropertyChanged("CircleRadius");
                    OnPropertyChanged("CircleLeft");
                    OnPropertyChanged("CircleTop");

                }
            }
        }
        public string Length
        {
            get { return _length.ToString(); }
            set
            {
                if (_length.ToString() != value)
                {
                    if (value.Length != 0)
                        _length = double.Parse(value);
                    else
                        _length = 0;
                    _lengthFixed = _length * 20;

                    BoxDelta = CircleX + CircleRadius / 2 + _lengthFixed / 2;
                    UpdateBoxPosition();
                    OnPropertyChanged("Length");
                    OnPropertyChanged("BoxDelta");
                }
            }
        }
        public string Velocity
        {
            get { return _velocity.ToString(); }
            set
            {
                if (_velocity.ToString() != value)
                {
                    if (value.Length != 0)
                        _velocity = double.Parse(value);
                    else
                        _velocity = 0;
                    OnPropertyChanged("Velocity");
                }
            }
        }
        public string Epsilon
        {
            get { return _epsilon.ToString(); }
            set
            {
                if (_epsilon.ToString() != value)
                {
                    if (value.Length != 0)
                        _epsilon = double.Parse(value);
                    else
                        _epsilon = 0;
                    OnPropertyChanged("Epsilon");
                }
            }
        }
        public double CircleRadius
        {
            get { return _radius * 40; }
            set
            {
                if (_radius != value)
                {
                    _radius = value;
                    OnPropertyChanged("CircleRadius");
                }
            }
        }
        public double CircleLeft
        {
            get { return CircleX - _radius * 20; }
            set
            {
                if (_radius != value)
                {
                    _radius = value;
                    OnPropertyChanged("CircleLeft");
                }
            }
        }
        public double CircleTop
        {
            get { return CircleY - _radius * 20; }
            set
            {
                if (_radius != value)
                {
                    _radius = value;
                    OnPropertyChanged("CircleTop");
                }
            }
        }
        public double BoxLeft
        {
            get { return _boxX; }
            set
            {
                if (_boxX != value)
                {
                    _boxX = value;
                    OnPropertyChanged("BoxLeft");
                }
            }
        }
        public double BoxDelta
        {
            get { return _boxDelta; }
            set
            {
                if (_boxDelta != value)
                {
                    _boxDelta = value;
                    OnPropertyChanged("BoxDelta");
                }
            }
        }

        public double M1X
        {
            get { return _m1x; }
            set
            {
                _m1x = value;
                OnPropertyChanged("M1X");
            }
        }
        public double M2X
        {
            get { return _m2x; }
            set
            {
                _m2x = value;
                OnPropertyChanged("M2X");
            }
        }
        public double M3X
        {
            get { return _m3x; }
            set
            {
                _m3x = value;
                OnPropertyChanged("M3X");
            }
        }
        public double M1Y
        {
            get { return _m1y; }
            set
            {
                _m1y = value;
                OnPropertyChanged("M1Y");
            }
        }
        public double M2Y
        {
            get { return _m2y; }
            set
            {
                _m2y = value;
                OnPropertyChanged("M2Y");
            }
        }
        public double M3Y
        {
            get { return _m3y; }
            set
            {
                _m3y = value;
                OnPropertyChanged("M3Y");
            }
        }
        public double MergeX
        {
            get { return _mergeX; }
            set
            {
                _mergeX = value;
                OnPropertyChanged("MergeX");
            }
        }
        public double MergeY
        {
            get { return _mergeY; }
            set
            {
                _mergeY = value;
                OnPropertyChanged("MergeY");
            }
        }
        public string ValueX
        {
            get { return _valueX.ToString(); }
            set
            {
                if (_valueX.ToString() != value)
                {
                    if (value.Length != 0)
                        _valueX = double.Parse(value);
                    else
                        _valueX = 0;
                    OnPropertyChanged("ValueX");
                }
            }
        }
        public string ValueXt
        {
            get { return _valueXt.ToString(); }
            set
            {
                if (_valueXt.ToString() != value)
                {
                    if (value.Length != 0)
                        _valueXt = double.Parse(value);
                    else
                        _valueXt = 0;
                    OnPropertyChanged("ValueXt");
                }
            }
        }
        public string ValueXtt
        {
            get { return _valueXtt.ToString(); }
            set
            {
                if (_valueXtt.ToString() != value)
                {
                    if (value.Length != 0)
                        _valueXtt = double.Parse(value);
                    else
                        _valueXtt = 0;
                    OnPropertyChanged("ValueXtt");
                }
            }
        }
        public string ValueTimer
        {
            get { return _timer.ToString(); }
            set
            {
                if (_timer.ToString() != value)
                {
                    if (value.Length != 0)
                        _timer = double.Parse(value);
                    else
                        _timer = 0;
                    OnPropertyChanged("ValueTimer");
                }
            }
        }

        #endregion
        #region COMMANDS
        private ICommand _startSymExecutedCommand, _stopSymExecutedCommand;

        public ICommand StartSimulation
        {
            get { return _startSymExecutedCommand ?? (_startSymExecutedCommand = new DelegateCommand(StartSymExecuted)); }
        }
        public ICommand StopSimulation
        {
            get { return _stopSymExecutedCommand ?? (_stopSymExecutedCommand = new DelegateCommand(StopSymExecuted)); }
        }
        #endregion
        #region COMMANDSExecuted

        private void StartSymExecuted()
        {
            _startTime = _valueTimePrev = DateTime.Now;
            SimulationStarted = false;

            _simulationTimer.Start();
        }
        private void StopSymExecuted()
        {
            _delay = 0;
            _simulationTimer.Stop();
            SimulationStarted = true;

            (ModelX.Series[0] as LineSeries).Points.Clear();
            ModelX.InvalidatePlot(true);
            foreach (var axis in ModelX.Axes)
                axis.Reset();

            (ModelXt.Series[0] as LineSeries).Points.Clear();
            ModelXt.InvalidatePlot(true);
            foreach (var axis in ModelXt.Axes)
                axis.Reset();

            (ModelXtt.Series[0] as LineSeries).Points.Clear();
            ModelXtt.InvalidatePlot(true);
            foreach (var axis in ModelXtt.Axes)
                axis.Reset();

            (ModelXXt.Series[0] as LineSeries).Points.Clear();
            (ModelXXt.Series[1] as LineSeries).Points.Clear();
            ModelXXt.InvalidatePlot(true);
            foreach (var axis in ModelXXt.Axes)
                axis.Reset();
        }

        #endregion
        public ViewModel()
        {
            _simulationTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 1) };
            _simulationTimer.Tick += _simulationTimer_Tick;
            generator = new GaussianRandom();
            PrepareStartValues();
        }
        private void PrepareStartValues()
        {
            _radius = 5;
            _length = 6;
            _lengthFixed = 120;
            _velocity = 50;
            _epsilon = 0.05;
            _boxX = 0;
            _simulationNotStarted = true;
            _rotationMatrix = Matrix.Identity;
            PrepareCirclesMiddle();
            RefreshCirclesMiddle();
            UpdateBoxPosition();
            PreparePlots();

            BoxDelta = CircleX + CircleRadius / 2 + _lengthFixed / 2;
            _valueX = BoxLeft - BoxDelta;

        }
        private void PreparePlots()
        {
            ModelX = new PlotModel();
            ModelXt = new PlotModel();
            ModelXtt = new PlotModel();
            ModelXXt = new PlotModel();

            ModelX.LegendBorderThickness = 0.5;
            ModelXt.LegendBorderThickness = 0.5;
            ModelXtt.LegendBorderThickness = 0.5;
            ModelXXt.LegendBorderThickness = 0.5;
            ModelX.LegendBorder = OxyColors.Black;
            ModelXt.LegendBorder = OxyColors.Black;
            ModelXtt.LegendBorder = OxyColors.Black;
            ModelXXt.LegendBorder = OxyColors.Black;

            ModelX.Series.Add(new LineSeries());
            (ModelX.Series[0] as LineSeries).Color = OxyColor.FromRgb(0, 188, 255);
            (ModelX.Series[0] as LineSeries).Title = "X";

            ModelXt.Series.Add(new LineSeries());
            (ModelXt.Series[0] as LineSeries).Color = OxyColor.FromRgb(160, 250, 85);
            (ModelXt.Series[0] as LineSeries).Title = "Xt";

            ModelXtt.Series.Add(new LineSeries());
            (ModelXtt.Series[0] as LineSeries).Color = OxyColor.FromRgb(250, 115, 85);
            (ModelXtt.Series[0] as LineSeries).Title = "Xtt";

            ModelXXt.Series.Add(new LineSeries());
            (ModelXXt.Series[0] as LineSeries).Color = OxyColor.FromRgb(255, 155, 0);
            (ModelXXt.Series[0] as LineSeries).Title = "Trajektoria";
            ModelXXt.Series.Add(new LineSeries());
            (ModelXXt.Series[1] as LineSeries).Color = OxyColor.FromRgb(26, 80, 170);
        }
        private void RefreshPlots(double time)
        {
            foreach (LineSeries s in ModelX.Series)
                if (s.Points.Count >= 500)
                    s.Points.RemoveAt(0);
            foreach (LineSeries s in ModelXt.Series)
                if (s.Points.Count >= 500)
                    s.Points.RemoveAt(0);
            foreach (LineSeries s in ModelXtt.Series)
                if (s.Points.Count >= 500)
                    s.Points.RemoveAt(0);
            if ((ModelXXt.Series[0] as LineSeries).Points.Count >= 800)
                (ModelXXt.Series[0] as LineSeries).Points.RemoveAt(0);
            if ((ModelXXt.Series[1] as LineSeries).Points.Count >= 3)
                (ModelXXt.Series[1] as LineSeries).Points.RemoveAt(0);

            (ModelX.Series[0] as LineSeries).Points.Add(new DataPoint(time, _valueX));
            ModelX.InvalidatePlot(true);

            if (_delay > 1)
            {
                (ModelXt.Series[0] as LineSeries).Points.Add(new DataPoint(time, _valueXt));
                ModelXt.InvalidatePlot(true);

                (ModelXXt.Series[0] as LineSeries).Points.Add(new DataPoint(_valueX, _valueXt));
                (ModelXXt.Series[1] as LineSeries).Points.Add(new DataPoint(_valueX, _valueXt));
                ModelXXt.InvalidatePlot(true);
            }
            if (_delay > 2)
            {
                (ModelXtt.Series[0] as LineSeries).Points.Add(new DataPoint(time, _valueXtt));
                ModelXtt.InvalidatePlot(true);
            }

            _delay++;
        }


        private double UpdateBoxPosition()
        {
            var l = _lengthFixed + generator.NextNormal(0, _epsilon);
            var underSqrt = l * l - (CircleY - MergeY) * (CircleY - MergeY);
            BoxLeft = MergeX + Math.Sqrt(underSqrt);
            return l;
        }

        private double UpdateBoxPosition2(double l, double mx, double my)
        {
            var underSqrt = l * l - (CircleY - my) * (CircleY - my);
            return mx + Math.Sqrt(underSqrt);
        }
        private void PrepareCirclesMiddle()
        {
            _m1xOryg = CircleX + Math.Cos(90 * Math.PI / 180);
            _m1yOryg = CircleY + Math.Sin(90 * Math.PI / 180);
            _m2xOryg = CircleX + Math.Cos(180 * Math.PI / 180);
            _m2yOryg = CircleY + Math.Sin(180 * Math.PI / 180);
            _m3xOryg = CircleX + Math.Cos(270 * Math.PI / 180);
            _m3yOryg = CircleY + Math.Sin(270 * Math.PI / 180);
            _mergeXOryg = CircleX + Math.Cos(0 * Math.PI / 180);
            _mergeYOryg = CircleY + Math.Sin(0 * Math.PI / 180);

            middles = new Point[]
           {
               new Point(_m1xOryg,_m1yOryg),
               new Point(_m2xOryg,_m2yOryg),
               new Point(_m3xOryg,_m3yOryg),
               new Point(_mergeXOryg,_mergeYOryg),
           };
        }

        private void RefreshCirclesMiddle()
        {
            Point[] tmp = new Point[middles.Length];
            middles.CopyTo(tmp, 0);
            var scaleMatrix = new Matrix();
            scaleMatrix.ScaleAt(CircleRadius / 2, CircleRadius / 2, CircleX, CircleY);
            scaleMatrix.Transform(tmp);

            M1X = tmp[0].X;
            M1Y = tmp[0].Y;
            M2X = tmp[1].X;
            M2Y = tmp[1].Y;
            M3X = tmp[2].X;
            M3Y = tmp[2].Y;
            MergeX = tmp[3].X;
            MergeY = tmp[3].Y;
        }
        private void RefreshValues(double time, double l)
        {
            _valueXPrev = _valueX;
            _valueX = BoxLeft - BoxDelta;
            _valueXt = (_valueX - _valueXPrev) / time;
          //  _valueXPrevPrev = CalculateNextPosition(time, l,true) - BoxDelta;
            var valueXNext = CalculateNextPosition(time, l, false) - BoxDelta;
           _valueXtt = (valueXNext - 2 * _valueX + _valueXPrev) / (time * time);
            OnPropertyChanged("ValueX");
            OnPropertyChanged("ValueXt");
            OnPropertyChanged("ValueXtt");
        }

        private double CalculateNextPosition(double time, double l, bool option)
        {
            var middle2 = new Point[middles.Length];
            middles.CopyTo(middle2, 0);

            var rot = Matrix.Identity;
 
            if (option)
                rot.RotateAt(_velocity * -2.0 * time, CircleX, CircleY);
            else
                rot.RotateAt(_velocity * time, CircleX, CircleY);
            rot.Transform(middle2);

            var scaleMatrix = new Matrix();
            scaleMatrix.ScaleAt(CircleRadius / 2, CircleRadius / 2, CircleX, CircleY);
            scaleMatrix.Transform(middle2);

            return UpdateBoxPosition2(l, middle2[3].X, middle2[3].Y);
        }

        private void _simulationTimer_Tick(object sender, EventArgs e)
        {
            var now = DateTime.Now;
            var time = (now - _valueTimePrev).TotalSeconds;
            _rotationMatrix = Matrix.Identity;
            _rotationMatrix.RotateAt(_velocity * time, CircleX, CircleY);
            _rotationMatrix.Transform(middles);
            RefreshCirclesMiddle();
            var l = UpdateBoxPosition();
            RefreshValues(time, l);
            _valueTimePrev = now;

            //timer liczacy uplyw czasu od Startu
            _timer = (now - _startTime).TotalSeconds;
            RefreshPlots(_timer);
            OnPropertyChanged("ValueTimer");
        }
    }
}
