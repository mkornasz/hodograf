﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace PSW1.Helpers
{
    class CoordsToCenterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int ellipseWidth = int.Parse(parameter.ToString());
            double ellipseCoord= double.Parse(value.ToString());
            return (double)(ellipseCoord - (ellipseWidth / 2));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
